# innkeeper-crud-api

Requires: Java 8 Maven > 3.3.2

Expose port: 1234

Build with: mvn clean package

Run with: java -jar innkeeper-crud-api.jar


Endpoint to udpate quality of Item: 
http://localhost:1234/v1/innkeeperservice/futurevalue/{days}

Endpoint to see all item in DB:
http://localhost:1234/v1/innkeeperservice/getall
