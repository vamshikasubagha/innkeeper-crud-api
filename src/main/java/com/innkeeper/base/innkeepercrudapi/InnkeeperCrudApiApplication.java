package com.innkeeper.base.innkeepercrudapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackages ="com.innkeeper.base")
public class InnkeeperCrudApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InnkeeperCrudApiApplication.class, args);
	}
}
