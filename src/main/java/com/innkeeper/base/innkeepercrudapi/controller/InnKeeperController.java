package com.innkeeper.base.innkeepercrudapi.controller;

import com.innkeeper.base.innkeepercrudapi.dto.Item;
import com.innkeeper.base.innkeepercrudapi.dto.ItemRepository;
import com.innkeeper.base.innkeepercrudapi.service.InnKeeperService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/innkeeperservice")
public class InnKeeperController {
 
	private static final Logger LOG = Logger.getLogger(InnKeeperController.class);
	
	@Autowired
	private Item item;

	@Autowired
	private InnKeeperService service;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@GetMapping("/getall")
	public List<Item> getAllItems() {
	    return itemRepository.findAll();
	}

	@RequestMapping(value="/getitem/{id}", method = RequestMethod.GET)
	public Item getItem(@PathVariable Long id) {
		LOG.debug("Number of id :"+ id);
		return itemRepository.getOne(id);
	}
	
	@PostMapping("/additem")
	public void addItem(@RequestBody Item body) {
		// Create the item if does not exist
		LOG.debug("body :"+ body);
		itemRepository.save(body);
	}

	@RequestMapping(value = "/futurevalue/{days}", method= RequestMethod.GET)
	public List<String> innUpdate(@PathVariable int days) {
		LOG.debug("Number of days :"+ days);
		List<String> result = service.updateDays(days);
		return result;
	}

	@RequestMapping(value = "/deleteitem/{id}", method= RequestMethod.DELETE)
	public  ResponseEntity<Item>  deleteItem(@PathVariable("id") Long id) {
		// Create the item if does not exist
		LOG.warn("Item being deleted :"+ id);
		itemRepository.deleteById(id);
		return new ResponseEntity<Item>(HttpStatus.NO_CONTENT);
	}

}
