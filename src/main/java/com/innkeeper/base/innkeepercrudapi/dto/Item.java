package com.innkeeper.base.innkeepercrudapi.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Component
@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Item implements Serializable {
	
	@Id
	@GeneratedValue
	private long id;
	
    public String Name;
    public int SellIn = 1;
    public int Quality = 1;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}
	public int getSellIn() {
		return SellIn;
	}
	public void setSellIn(int sellIn) {
		SellIn = sellIn;
	}
	public int getQuality() {
		return Quality;
	}
	public void setQuality(int quality) {
		Quality = quality;
	}
	
	@Override
	@JsonProperty
	public String toString() {
		return " Item [id= " + id + ", Name= " + Name + ", SellIn= " + SellIn + ", Quality= " + Quality + "]";
	}

	public Item clone(){
		Item it = new Item();
		it.setName(this.getName());
		it.setSellIn(this.getSellIn());
		it.setQuality(this.getQuality());
		it.setId(this.getId());
		return it;
	}
}
