package com.innkeeper.base.innkeepercrudapi.dto;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long>{

	}







//Item findByName(String Name);
//List<Item> findByName(String name);
//
// @Modifying
// @Transactional
// @Query("delete from Item u where u.id = {id}")
// void deleteItem();