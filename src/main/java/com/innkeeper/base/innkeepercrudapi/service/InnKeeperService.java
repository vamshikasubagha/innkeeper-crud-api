package com.innkeeper.base.innkeepercrudapi.service;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.innkeeper.base.innkeepercrudapi.dto.Item;
import com.innkeeper.base.innkeepercrudapi.dto.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by kasubaghasridvr on 4/7/18.
 */
//@EnableJpaRepositories (repositoryBaseClass = ItemRepository.class)

//@Transactional
@Service
@JsonSerialize
public class InnKeeperService {

    @Autowired
    private ItemRepository itemRepository;

    List<String> result = new ArrayList<>();


    List<Item> Items = new ArrayList<Item>();


    public InnKeeperService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<String> updateDays(int days){
        Items =  itemRepository.findAll();
        result.clear();
        for(int i=0;i<days;i++) {
            Items = updateQualityItem(i);
            int day = i+1;
            for(Item it : Items)
                result.add("Day : " + day + it.toString());
        }
        return result;
    }


    public List<Item> updateQualityItem(int day) {
        try {
            for (int i = 0; i < day; i++)
            {
                if (Items.get(i).Name != "Aged Brie" && Items.get(i).Name != "Backstage passes to a TAFKAL80ETC concert" )
                {
                    if (Items.get(i).Quality > 0)
                    {
                        if (Items.get(i).Name != "Sulfuras, Hand of Ragnaros" && Items.get(i).Name != "Conjured Mana Cake" )
                        {
                            Items.get(i).Quality = Items.get(i).Quality - 1;
                        } else if(Items.get(i).Name == "Conjured Mana Cake") {
                            Items.get(i).Quality =   Math.max(Items.get(i).Quality - 2, 0);

                        }
                    }
                }
                else
                {
                    if (Items.get(i).Quality < 50)
                    {
                        Items.get(i).Quality = Items.get(i).Quality + 1;

                        if (Items.get(i).Name == "Backstage passes to a TAFKAL80ETC concert")
                        {
                            if (Items.get(i).SellIn < 11)
                            {
                                if (Items.get(i).Quality < 50)
                                {
                                    Items.get(i).Quality = Items.get(i).Quality + 1;
                                }
                            }

                            if (Items.get(i).SellIn < 6)
                            {
                                if (Items.get(i).Quality < 50)
                                {
                                    Items.get(i).Quality = Items.get(i).Quality + 1;
                                }
                            }
                        }
                    }
                }

                if (Items.get(i).Name != "Sulfuras, Hand of Ragnaros")
                {
                    Items.get(i).SellIn = Items.get(i).SellIn - 1;
                }

                if (Items.get(i).SellIn < 0)
                {
                    if (Items.get(i).Name != "Aged Brie")
                    {
                        if (Items.get(i).Name != "Backstage passes to a TAFKAL80ETC concert")
                        {
                            if (Items.get(i).Quality > 0)
                            {
                                if (Items.get(i).Name != "Sulfuras, Hand of Ragnaros" &&  Items.get(i).Name != "Conjured Mana Cake")
                                {
                                    Items.get(i).Quality = Items.get(i).Quality - 1;
                                } else if(Items.get(i).Name == "Conjured Mana Cake") {
                                    Items.get(i).Quality =   Math.max(Items.get(i).Quality - 2, 0);
                                }
                            }
                        }
                        else
                        {
                            Items.get(i).Quality =  Items.get(i).Quality -  Items.get(i).Quality;
                        }
                    }
                    else
                    {
                        if ( Items.get(i).Quality < 50)
                        {
                            Items.get(i).Quality =  Items.get(i).Quality + 1;

                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Caught exception");
        }
        return Items;
    }



    public ItemRepository getItemRepository() {
        return itemRepository;
    }

    public void setItemRepository(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> getItems() {
        return Items;
    }

    public void setItems(List<Item> items) {
        Items = items;
    }


}
