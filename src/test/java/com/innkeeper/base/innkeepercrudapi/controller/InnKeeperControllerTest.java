package com.innkeeper.base.innkeepercrudapi.controller;

import com.innkeeper.base.innkeepercrudapi.dto.Item;
import com.innkeeper.base.innkeepercrudapi.dto.ItemRepository;
import com.innkeeper.base.innkeepercrudapi.service.InnKeeperService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by kasubaghasridvr on 4/10/18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {InnKeeperController.class })
@Component
public class InnKeeperControllerTest {

    @MockBean
    private InnKeeperService mockinnKeeperService;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private Item item;

    @Configuration
    @EnableAutoConfiguration
    @Import({InnKeeperController.class,InnKeeperService.class,Item.class})
    public static class TestApplication {

    }


    @Test
    public void testgetAllItems() throws Exception{
        assertEquals(true,true);
    }

}